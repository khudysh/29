import Header from './Components/Header'
import Compet from './Components/Compet'
import NavBar from './Components/NavBar'
import Support from './Components/Support'
import Expertise from './Components/Expertise'
import Tariffs from './Components/Tariffs'
import Cases from './Components/Cases'
import Team from './Components/Team'
import Partnership from './Components/Partnership'
import Reviews from './Components/Reviews'
import Footer from './Components/Footer'
import Solution from './Components/Solution'

function App() {
  return (
    <>
    <NavBar />
    <Header />
    <Compet/>
    <Support/>
    <Expertise/>
    <Tariffs />
    <Solution />
    <Cases />
    <Team />
    <Reviews />
    <Partnership />
    <Footer/>
    </>
  )
}

export default App;
